from django.urls import path

from users import views


urlpatterns = [
    path('users/login', views.UserLogin.as_view()),
    path('users/logout', views.UserLogout.as_view()),
    path('users/refresh-token', views.TokenRefresh.as_view()),
    path('users/profile', views.UserProfile.as_view()),
    path('users/staff/profile', views.StaffProfile.as_view()),
]