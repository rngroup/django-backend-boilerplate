from rest_framework import serializers


class UserLoginSerializer(serializers.Serializer):
    user_id = serializers.CharField(max_length=100)
    password = serializers.CharField(max_length=100)


class TokenRefreshSerializer(serializers.Serializer):
    refresh = serializers.CharField(max_length=500)