from django.db import models
from django.contrib.auth.models import AbstractBaseUser, AbstractUser


class UserProfile(AbstractBaseUser):
    user_id = models.CharField(
        verbose_name='User ID',
        max_length=10,
        unique=True,
    )
    name = models.CharField(max_length=100)
    email = models.EmailField(
        verbose_name='Email Address',
        max_length=255,
        unique=True,
    )

    USERNAME_FIELD = 'user_id'

    class Meta:
        verbose_name = "User Profile"
        verbose_name_plural = "User Profiles"

    def __str__(self):
        return self.user_id


class StaffProfile(AbstractUser):
    user_id = models.CharField(
        verbose_name='User ID',
        max_length=10,
        unique=True,
    )
    staff_id = models.CharField(
        verbose_name='Staff ID',
        max_length=10,
        unique=True,
    )

    class Meta:
        verbose_name = "Staff Profile"
        verbose_name_plural = "Staff Profiles"

    def __str__(self):
        return self.username


class TokenProfile(models.Model):
    user_id = models.CharField(verbose_name='User ID', max_length=10)
    token_id = models.CharField(
        verbose_name='Token ID',
        max_length=255,
        unique=True
    )
    created_at = models.DateTimeField()
    expires_at = models.DateTimeField()
    is_logged_out = models.BooleanField(default=False)
    is_blacklisted = models.BooleanField(default=False)
    blacklisted_at = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.token_id
