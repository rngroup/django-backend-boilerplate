from django.contrib import admin

from .models import UserProfile, StaffProfile, TokenProfile


class UserProfileAdmin(admin.ModelAdmin):
    pass


class StaffProfileAdmin(admin.ModelAdmin):
    pass


class TokenProfileAdmin(admin.ModelAdmin):
    pass


admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(StaffProfile, StaffProfileAdmin)
admin.site.register(TokenProfile, TokenProfileAdmin)
