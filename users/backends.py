from django.contrib.auth.backends import ModelBackend

from users.models import UserProfile


class UserProfileBackend(ModelBackend):

    def authenticate(self, request, **kwargs):
        user_id = kwargs['username']
        password = kwargs['password']
        user = UserProfile.objects.filter(user_id=user_id).first()
        if user and user.check_password(password):
            return user