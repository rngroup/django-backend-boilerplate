from datetime import datetime
from django.contrib.auth import authenticate
from django.utils import timezone
from rest_framework.exceptions import AuthenticationFailed, ValidationError
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.exceptions import TokenError
from rest_framework_simplejwt.tokens import RefreshToken
from backend.permissions import IsUserAuthenticated, IsStaffAuthenticated
from users.models import TokenProfile
from users.serializers import UserLoginSerializer, TokenRefreshSerializer


class UserLogin(APIView):
    def post(self, request):
        serializer = UserLoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = authenticate(username=serializer.data['user_id'],
                            password=serializer.data['password'])
        if user:
            refresh = RefreshToken.for_user(user)
            TokenProfile.objects.create(
                user_id=user.user_id,
                token_id=refresh.payload['jti'],
                created_at=timezone.make_aware(datetime.fromtimestamp(refresh.payload['iat'])),
                expires_at=timezone.make_aware(datetime.fromtimestamp(refresh.payload['exp']))
            )
            return Response({
                'access': str(refresh.access_token),
                'refresh': str(refresh)
            })
        else:
            raise AuthenticationFailed({
                'message': 'user id or password mismatch'
            })


class UserLogout(APIView):
    def post(self, request):
        serializer = TokenRefreshSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            refresh = RefreshToken(serializer.data['refresh'])
            refresh_token = TokenProfile.objects.filter(token_id=refresh.payload['jti']).first()
            if refresh_token:
                refresh_token.is_logged_out = True
                refresh_token.is_blacklisted = True
                refresh_token.blacklisted_at = timezone.now()
                refresh_token.save()
                return Response({
                    'message': 'successfully logged out'
                })
            raise TokenError()
        except TokenError:
            raise ValidationError({
                'message': 'token is invalid or expired'
            })


class TokenRefresh(APIView):
    def post(self, request):
        serializer = TokenRefreshSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            refresh = RefreshToken(serializer.data['refresh'])
            blacklisted_token = TokenProfile.objects.filter(token_id=refresh.payload['jti']).first()
            if not blacklisted_token or blacklisted_token.is_blacklisted:
                raise TokenError()
        except TokenError:
            raise ValidationError({
                'message': 'token is invalid or expired'
            })
        return Response({
            'access': str(refresh.access_token)
        })


class UserProfile(APIView):

    permission_classes = [IsUserAuthenticated]

    def get(self, request):
        return Response({
            'user_id': request.user.user_id,
            'name': request.user.name
        })


class StaffProfile(APIView):

    permission_classes = [IsStaffAuthenticated]

    def get(self, request):
        return Response({
            'user_id': request.user.user_id,
            'name': request.user.first_name
        })
