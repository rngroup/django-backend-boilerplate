from rest_framework.permissions import BasePermission

from users.models import UserProfile, StaffProfile


class IsUserAuthenticated(BasePermission):

    def has_permission(self, request, view):
        if request.user.is_authenticated:
            request.user = UserProfile.objects.filter(user_id=request.user.user_id).first()
            if not request.user:
                return False
            return super().has_permission(request, view)


class IsStaffAuthenticated(BasePermission):

    def has_permission(self, request, view):
        if request.user.is_authenticated:
            request.user = StaffProfile.objects.filter(user_id=request.user.user_id).first()
            if not request.user or not request.user.is_staff or not request.user.is_active:
                return False
            return super().has_permission(request, view)